package com.example.payasu.memory;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.payasu.memory.Control.Configuracio;
import com.example.payasu.memory.Joc.OnePlayerDifficult;
import com.example.payasu.memory.Joc.OnePlayerEasy;
import com.example.payasu.memory.Joc.TwoPlayers;

public class MainActivity extends Activity {

    Button playButton, configButton;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setStartButton();
        setPreferenceButton();
        setEasyButton();
        setDifiButton();
    }



    private void setStartButton() {
        playButton = (Button)findViewById(R.id.buttonPlay);
        playButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startGame();

            }
        });
    }

    private void setEasyButton() {
        configButton = (Button)findViewById(R.id.buttonPlayEasy);
        configButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startGameEasy();
            }
        });
    }

    private void setDifiButton() {
        configButton = (Button)findViewById(R.id.buttonPlayDificil);
        configButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startGameDificil();
            }
        });
    }

    private void setPreferenceButton() {
        configButton = (Button)findViewById(R.id.buttonConfig);
        configButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                showPreferences();
            }
        });
    }

    protected void startGame() {
        Intent game = new Intent(this, TwoPlayers.class);
        startActivityForResult(game, 1234);
    }

    protected void startGameEasy() {
        Intent game = new Intent(this, OnePlayerEasy.class);
        startActivityForResult(game, 1234);
    }

    protected void startGameDificil() {
        Intent game = new Intent(this, OnePlayerDifficult.class);
        startActivityForResult(game, 1234);
    }

    protected void showPreferences() {
        Intent i = new Intent(this, Configuracio.class);
        startActivity(i);
    }
}