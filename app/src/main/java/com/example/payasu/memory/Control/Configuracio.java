package com.example.payasu.memory.Control;
 
import android.os.Bundle;
import android.preference.PreferenceActivity;

import com.example.payasu.memory.R;


public class Configuracio extends PreferenceActivity {
	/**
	 * @see android.app.Activity#onCreate(Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.configuration);
	}
}
