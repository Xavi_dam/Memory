package com.example.payasu.memory.Control;

import android.content.Context;

public class VistaCarta extends android.support.v7.widget.AppCompatImageView {

	private int status; 
	private Integer imageID;
	private Integer imagebackID;
	
	public VistaCarta(Context context) {
		super(context);
		this.status = 0;
	}
	public void flipView(){
	    	if(status == 1) status = 0;
	    	else status = 1;
	    	actualizarView();
	}
   	public void actualizarView(){
   		if(status == 1) this.setImageResource(imageID);
    	else if(status == 0) this.setImageResource(imagebackID);
    	else this.setVisibility(VISIBLE);
   	}
   	public void setCardImages(Integer image, Integer imageBack){
   		this.imagebackID = imageBack;
   		this.imageID = image;
   	}
   	public Integer getImageID(){
   		return imageID;
   	}
   	public int getStatus(){
   		return status;
   	}
   	public void deleteCard(){
   		status = 2;
   		actualizarView();
   	}
}
