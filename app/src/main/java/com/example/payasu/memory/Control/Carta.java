package com.example.payasu.memory.Control;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.example.payasu.memory.R;

import java.util.Random;

public class Carta extends BaseAdapter {
    private Context mContext;
    private cardController cardControl;

    public Carta(Context c) {
        mContext = c;
        if(cardControl == null) cardControl = new cardController();
    }

    public int getCount() {
        return cardControl.getnumCards();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        VistaCarta card;
        if (convertView == null) {  // if it's not recycled, initialize some attributes
            card = new VistaCarta(mContext);
            card.setCardImages(cardControl.getCard(),cardControl.getCardBack());
            card.setLayoutParams(new GridView.LayoutParams(200, 250));
            card.setScaleType(ImageView.ScaleType.CENTER_CROP);
            card.setPadding(8, 8, 8, 8);
        } else {
            card = (VistaCarta) convertView;
            card.setCardImages(cardControl.getCard(), cardControl.getCardBack());
        }
        card.actualizarView();
        return card;
    }
 

    // references to our images
   
    
 
    private class cardController{
    	private Integer[] cards;
    	private Integer cardBack = R.drawable.back;
    	int index;
    	public cardController( ) {
    		index = 0;
    		cards  = new Integer[]  {
		            R.drawable.c0, R.drawable.c1, R.drawable.c2, R.drawable.c3, R.drawable.c4, R.drawable.c5,
					R.drawable.c6, R.drawable.c7, R.drawable.c8, R.drawable.c9, R.drawable.c0, R.drawable.c1,
                    R.drawable.c2, R.drawable.c3, R.drawable.c4, R.drawable.c5, R.drawable.c6, R.drawable.c7,
                    R.drawable.c8, R.drawable.c9,
            };
    		Random rgen = new Random();
    		for (int i=0; i<cards.length; i++) {
    		    int randomPosition = rgen.nextInt(cards.length);
    		    int temp = cards[i];
    		    cards[i] = cards[randomPosition];
    		    cards[randomPosition] = temp;
    		}
		}
		public  Integer getCard() {
			if(index<cards.length){
				Integer result = cards[index];
				index++;
				return result;
			}
			else{
				Integer result = cards[0];
				index=1; 
				return result;
			}
		}
		public Integer getCardBack(){
			return cardBack;
		}
		public int getnumCards(){
			return cards.length;
		}
    }
}
